package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Department;
import mx.unam.dgtic.dominio.Employee;

public class DepartmentService {

	protected EntityManager em;

	public DepartmentService(EntityManager em) {	
		this.em = em;
	}

	
	
	public Department createDepartment ( String name ) {
		
		Department d=new Department();
		//se comenta id al usar una estartegia de autogeneracion de llave

		d.setName(name);
	
		
		em.getTransaction().begin();
		em.persist(d);
		em.flush();
		em.getTransaction().commit();
		return d;
		
		
	}
	
	public Collection<Department> findAllDepartments() {
		
		Query query =em.createQuery("Select e FROM Department e");
		return (Collection<Department>)query.getResultList();
	}
	
	
}
