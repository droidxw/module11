package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.ParkingSpace;

public class ParkingSpaceService {
	protected EntityManager em;
	
	public ParkingSpaceService(EntityManager em) {
		this.em=em;
	}
	
	public ParkingSpace createParkingSpace (Employee emp, int lot, String location) {
		ParkingSpace space=new ParkingSpace();
		space.setId(emp.getId());
		space.setLot(lot);
		space.setLocation(location);
		
		em.getTransaction().begin();
		emp.setParkingSpace(space);
		space.setEmployee(emp);
		em.persist(space);
		em.flush();
		em.getTransaction().commit();;
		return space;
	}
	
	public Collection<ParkingSpace> findAllParkingSpaces(){
	Query query =em.createQuery("SELECT p FROM ParkingSpace p");
	return  (Collection<ParkingSpace>) query.getResultList();
	
	}
}
