package mx.unam.dgtic.dominio;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;

@Entity
public class Employee {
	
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private long salary;

//    @ManyToOne
//    //se agrega cuando no hay contarint de pk en la bd
//    @JoinColumn(name="DEPT_ID")(name="DEPT_ID")
//    private Department department;
    
//    @OneToOne
//    @JoinColumn(name="PSPACE_ID")
    
    @OneToOne(mappedBy ="employee")    
    private ParkingSpace parkingSpace;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }


    
//    public Department getDepartment() {
//		return department;
//	}
//
//	public void setDepartment(Department department) {
//		this.department = department;
//	}
    
    



	public ParkingSpace getParkingSpace() {
		return parkingSpace;
	}

	public void setParkingSpace(ParkingSpace parkinSpace) {
		this.parkingSpace = parkinSpace;
	}
	
	
	@Override
    public String toString() {
        return "Employee [id=" + id + ", name=" + name + ", salary=" + salary +", ParkingSpace= "+getParkingSpace() +"]";
    }

}