package mx.unam.dgtic.main;

import java.util.Collection;

import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.ParkingSpace;
import mx.unam.dgtic.modelo.EmployeeService;
import mx.unam.dgtic.modelo.ParkingSpaceService;

public class PrincipalEmployeeParkingSpace {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
	EmployeeService service =new EmployeeService(em);
	ParkingSpaceService pService =new ParkingSpaceService(em);

	Employee emp =service.createEmployee("HectordelaTorre", 1000);
	pService.createParkingSpace(emp, 1001, "TIC1000");

		
		Collection<Employee> empleados=service.findAllEmployees();
		
		System.out.println("Empleados localizados local");
		System.out.println("HectordelaTorre");
		for(Employee e:empleados) {
		System.out.println(e);		
		
		}

		Collection<ParkingSpace> pSpaces = pService.findAllParkingSpaces();
		
		if (pSpaces.isEmpty()) {
			System.out.println("ParkingSpaces not found");
		}else {
			
			System.out.println("ParkingSpaces localizados local");
		}
		for(ParkingSpace p:pSpaces) {
		System.out.println(p);	
		System.out.println("owner"+p.getEmployee());	

		}
	}
	
	
	

}
