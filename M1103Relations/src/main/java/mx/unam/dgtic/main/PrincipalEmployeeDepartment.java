package mx.unam.dgtic.main;

import java.util.Collection;

import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Alumno;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.modelo.EmployeeService;

public class PrincipalEmployeeDepartment {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
	EmployeeService service =new EmployeeService(em);
//	Employee emp =service.createEmployee(402, "Omar", 4000);
	Employee emp =service.createEmployee("Hector dela Torre", 4000);
	System.out.println("Creado: " +emp);
	
//	emp=service.setEmployeeDepartment(402, 4);
	
//	service quita al remover department
//	emp=service.setEmployeeDepartment(emp.getId(), 1);
	
		Collection<Employee> empleados=service.findAllEmployees();
		
		System.out.println("Employe localizados Local");
		for(Employee e:empleados)
		System.out.println(e);


		
	}

}
