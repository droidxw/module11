package mx.unam.dgtic.dominio;

import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue("FTEmp")
public class FullTimeEmployee extends CompanyEmployee{
	
	private long salary;
	private long pension;
	
	
	public FullTimeEmployee() {
		super();
	}



	public FullTimeEmployee(long salary, long pension) {
		
		this.salary = salary;
		this.pension = pension;
	}
	
	
	
	public long getSalary() {
		return salary;
	}
	public void setSalary(long salary) {
		this.salary = salary;
	}
	public long getPension() {
		return pension;
	}
	public void setPension(long pension) {
		this.pension = pension;
	}
	
	
	@Override
    public String toString() {
        return "FullTimeEmployee [" + "id: " + getId() + " name: " + getName()
                + ", StartDate: " + getStartDate()
                + ", Vacation=" + getVacation()
                + ", Salary: " + salary + ", Pension: " + pension + "]";
    }

	

}
