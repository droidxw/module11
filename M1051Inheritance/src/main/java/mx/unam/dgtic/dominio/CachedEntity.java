package mx.unam.dgtic.dominio;

public abstract class CachedEntity {
    private long createTime;

    public CachedEntity() {
        createTime = System.currentTimeMillis();
    }

    public long getCacheAge() {
        return System.currentTimeMillis() - createTime;
    }
}
