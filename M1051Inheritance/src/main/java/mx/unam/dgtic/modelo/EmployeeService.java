package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Employee;

public class EmployeeService {

	protected EntityManager em;

	public EmployeeService(EntityManager em) {
		this.em = em;
	}

	public Employee createEmployee(Employee emp) {

		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;

	}

	public Collection<Employee> findAllEmployees() {

		Query query = em.createQuery("Select e FROM Employee e", Employee.class);
		return (Collection<Employee>) query.getResultList();
	}
//
}
