package mx.unam.dgtic.main;

import java.util.Collection;

import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.ContractEmployee;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.FullTimeEmployee;
import mx.unam.dgtic.dominio.PartTimeEmployee;
import mx.unam.dgtic.modelo.EmployeeService;

public class PrincipalEmployee {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
	EmployeeService service =new EmployeeService(em);
	

	System.out.println("Hector de la Torre");
	System.out.println("");
	
	Employee emp = null;

	String tipo = "Contract";
	if (tipo.equals("Full")) {
//		valores para constructor
	    emp = new FullTimeEmployee(1000,10000);
	} else if (tipo.equals("Part")) {
	    emp = new PartTimeEmployee(100.10f);
	} else {
	    emp = new ContractEmployee(1200,34);
	}
	emp.setId(1003);
	emp.setName("RAUL PEREZ");
	service.createEmployee(emp);

	
		
		Collection<Employee> empleados=service.findAllEmployees();
		
		System.out.println("Employe localizados Local");
		for(Employee e:empleados)
		System.out.println(e);
//		System.out.println(e.getConvertedName());
//		System.out.println(e.getName());

		
	}

}
