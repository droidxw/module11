package mx.unam.dgtic.main;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Alumno;
import mx.unam.dgtic.modelo.AlumnoService;

public class Principal {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
		AlumnoService service =new AlumnoService(em);
		
		Collection<Alumno> alumnos=service.findAllAlumnos();
		
		System.out.println("Alumnos localizados Local");
		for(Alumno a:alumnos)
		System.out.println(a);
		
		System.out.println("--");

		//		**
//		EntityManagerFactory emfa=Persistence.createEntityManagerFactory("arceliabase");
//		EntityManager ema=emfa.createEntityManager();
		
		
//		AlumnoService servicea =new AlumnoService(ema);
		
		
		emf=Persistence.createEntityManagerFactory("arceliabase");
		
		em=emf.createEntityManager();
		service =new AlumnoService(em);
		Collection<Alumno> alumnosa=service.findAllAlumnos();
		
		System.out.println("Alumnos localizados Arcelia");
		for(Alumno a:alumnosa)
		System.out.println(a);
		
	}

}
