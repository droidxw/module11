package mx.unam.dgtic.main;

import java.util.Collection;

import com.google.protobuf.Service;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.Project;
import mx.unam.dgtic.modelo.EmployeeService;
import mx.unam.dgtic.modelo.ProjectService;

public class PrincipalEmployeeProject {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("m11base");
		// administrar ciclo de vida de unidad
		EntityManager em = emf.createEntityManager();
		//para limpiar buffer alta cocurrencia(transaccionalidad) nos puede ocasionar problemas de desempeño
//		em.getTransaction().begin();
//		em.flush();
//		em.getTransaction().commit();
		
		EmployeeService service = new EmployeeService(em);
		ProjectService pService = new ProjectService(em);
//	Employee emp =service.createEmployee(402, "Omar", 4000);
	Employee emp =service.createEmployee("Hector dela Torre", 4000);
	System.out.println("Creado: " +emp);

//	emp=service.setEmployeeDepartment(402, 4);

//	service quita al remover department
	Project proj=pService.createProject("M11");
	emp=service.setEmployeeProject(emp.getId(), proj.getId());
		
//		service.setEmployeeProject(200, 5);

		Collection<Employee> empleados = service.findAllEmployees();
		Collection<Project> projects;
		System.out.println("Employe localizados Local");
		for (Employee e : empleados) {
			System.out.println(e);
			projects=e.getProjects();
			if(projects.isEmpty()) {
				System.out.println("Sin projects para el empleado " +e.getName());
				
			}else {
				
				System.out.println("Projects para el empleado " +e.getName());
				for(Project p:projects){
					System.out.println(p.getName());
					
				}
			}
			System.out.println(" ");
		}

		System.out.println("-----");

		projects = pService.findAllProjects();
		if (projects.isEmpty()) {
			System.out.println("projects no localizados");

		} else {
			System.out.println("projects localizados");
			for (Project p : projects) {

				System.out.println(p + "" + p.getEmployees().size() + " empleados");
				empleados = p.getEmployees();
				if (empleados.isEmpty()) {
					System.out.println("Sin empleados en el project " + p.getName());

				} else {
					System.out.println("Empleados en el project " + p.getName());
					for (Employee e : empleados) {

						System.out.println(e.getName());
					}
				}
				System.out.println("");
			}

		}
	}

}
