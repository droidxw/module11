package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.Project;

public class ProjectService {
	protected EntityManager em;

	public ProjectService(EntityManager em) {
		this.em = em;
	}

	public Project createProject(String name) {
		Project p = new Project();
		p.setName(name);
		em.getTransaction().begin();		
		em.persist(p);
		em.getTransaction().commit();
		return p;

	}

	public Collection<Project> findAllProjects() {

		Query query = em.createQuery("Select p FROM Project p");
		return (Collection<Project>) query.getResultList();
	}

}
