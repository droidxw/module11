package mx.unam.dgtic.dominio;

import java.util.ArrayList;
import java.util.Collection;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;

@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private long salary;

	@ManyToMany
	@JoinTable(name="EMP_PROJ", 
	joinColumns =@JoinColumn(name="EMP_ID"),
				inverseJoinColumns = @JoinColumn(name="PROJ_ID"))
			
	private Collection<Project> projects;

	public Employee() {
		projects = new ArrayList<Project>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public Collection<Project> getProjects() {
		return projects;
	}

	public void addProject(Project project) {
		if (!getProjects().contains(project)) {
			getProjects().add(project);

		}
		if (!project.getEmployees().contains(this)) {
			project.getEmployees().add(this);
		}

	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary 
				+", with = "+getProjects().size()+" projects"
				+ "] ";
	}

}