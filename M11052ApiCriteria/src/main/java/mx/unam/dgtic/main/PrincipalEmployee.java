package mx.unam.dgtic.main;

import java.util.Collection;

import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.modelo.EmployeeService;
import mx.unam.dgtic.modelo.SearchService;

public class PrincipalEmployee {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
	SearchService service =new 	SearchService(em);

	System.out.println("Hector de la Torre");
	System.out.println("CRITERIA");
	System.out.println("");
		
	System.out.println("Buscar empleado");
	String name = null;
	String dept = null;
	String project ="CRM";
	String city  = null;

	printCollection(service.findEmployees(name, dept, project, city));
	System.out.println("");

	System.out.println("Todos los empleados");
	printCollection(service.findAllEmployees());

		
	}
	
	private static void printCollection(Collection<?> c) {
        for (Object o : c) {
            System.out.println(o);
        }
    }


}
