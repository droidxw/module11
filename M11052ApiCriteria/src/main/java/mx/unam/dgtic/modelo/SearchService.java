package mx.unam.dgtic.modelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.ParameterExpression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.Project;

public class SearchService {

	protected EntityManager em;

	public SearchService(EntityManager em) {
		this.em = em;
	}

	public Employee createEmployee(int id, String name, long salary) {

		Employee emp = new Employee();

		emp.setId(id);
		emp.setName(name);
		emp.setSalary(salary);

		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;

	}

	public void removeEmployee(int id) {
		Employee emp = findEmployee(id);
		if (emp != null) {
			em.getTransaction().begin();
			em.remove(emp);
			em.getTransaction().commit();
		}
	}

	public Employee changeEmployeeSalary(int id, long newSalary) {
		Employee emp = findEmployee(id);
		if (emp != null) {
			em.getTransaction().begin();
			emp.setSalary(newSalary);
			em.getTransaction().commit();
		}
		return emp;
	}

	public Employee findEmployee(int id) {
		return em.find(Employee.class, id);
	}

	public List<Employee> findAllEmployees() {
		return em.createQuery("SELECT e FROM Employee e", Employee.class).getResultList();
	}

	public List<Employee> findEmployees(String name, String deptName, String projectName, String city) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Employee> c = cb.createQuery(Employee.class);
		Root<Employee> emp = c.from(Employee.class);
		c.select(emp);
		c.distinct(true);
		Join<Employee, Project> project = emp.join("projects", JoinType.LEFT);

		List<Predicate> criteria = new ArrayList<Predicate>();
		if (name != null) {
			ParameterExpression<String> p = cb.parameter(String.class, "name");
			criteria.add(cb.equal(emp.get("name"), p));
		}
		if (deptName != null) {
			ParameterExpression<String> p = cb.parameter(String.class, "dept");
			criteria.add(cb.equal(emp.get("department").get("name"), p));
		}
		if (projectName != null) {
			ParameterExpression<String> p = cb.parameter(String.class, "project");
			criteria.add(cb.equal(project.get("name"), p));
		}
		if (city != null) {
			ParameterExpression<String> p = cb.parameter(String.class, "city");
			criteria.add(cb.equal(emp.get("address").get("city"), p));
		}

		if (criteria.size() == 0) {
			throw new RuntimeException("no criteria");
		} else if (criteria.size() == 1) {
			c.where(criteria.get(0));
		} else {
			c.where(cb.and(criteria.toArray(new Predicate[0])));
		}

		TypedQuery<Employee> q = em.createQuery(c);
		if (name != null) {
			q.setParameter("name", name);
		}
		if (deptName != null) {
			q.setParameter("dept", deptName);
		}
		if (project != null) {
			q.setParameter("project", projectName);
		}
		if (city != null) {
			q.setParameter("city", city);
		}
		return q.getResultList();
	}

}
