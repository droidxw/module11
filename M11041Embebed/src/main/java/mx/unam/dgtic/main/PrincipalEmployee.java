package mx.unam.dgtic.main;

import java.util.Collection;

import javax.management.loading.PrivateClassLoader;
import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.modelo.EmployeeService;

public class PrincipalEmployee {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
	EmployeeService service =new EmployeeService(em);
	
//	solo se ejecuta una vez para que no marque error	
	//llamamos al metodo en el caso de autogenracion de llave para auto
//	service.createEmployee("Hector de la Torre", 5000, "Arboledas","Miguel Hidalgo", "CDMX","11900");
//	System.out.println("Hector de la Torre");
//	System.out.println("");
	
	
	service.createEmployeeAndAddress("Hector de la Torre", 5000, "Arboledas","Miguel Hidalgo", "CDMX","11900");
	System.out.println("Hector de la Torre");
	System.out.println("");
	
	System.out.println("Empleados localizados local");
	printCollection(service.findAllEmployees());
	
	System.out.println("Nueva compañia");
	//se comenta al ejecutar dos veces para evitar error de llave duplicada
//	service.createCompanyAndAddress( 5000, "Bosques","Benito Juarez", "CDMX","1130");
	
	System.out.println("CompañiS localizados local");
	printCollection(service.findAllCompanies());
		

//		System.out.println(e.getConvertedName());
//		System.out.println(e.getName());

		
	}
	
	private static void printCollection(Collection <?> c) {
	for (Object o:c) {
		System.out.println(o);
		
	}
	
}

}
