package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Address;
import mx.unam.dgtic.dominio.Company;
import mx.unam.dgtic.dominio.Employee;

public class EmployeeService {
	protected EntityManager em;

	public EmployeeService(EntityManager em) {
		this.em = em;
	}
	
	
	public Employee createEmployee(int id, String name, long salary) {
		Employee emp = new Employee();
		emp.setId(id);
		emp.setName(name);
		emp.setSalary(salary);
		
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		
		return emp;
	}
	
	public Employee createEmployee(String name, long salary) {
		Employee emp = new Employee();
		//Se usa cuando usamos una estrategia de autogeneracion de llave
		emp.setName(name);
		emp.setSalary(salary);
		
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		
		return emp;
	}
	
	public Employee createEmployeeAndAddress(String name, long salary,
			String street, String city, String state, String zip) {
		Employee emp = new Employee();
		
		emp.setName(name);
		emp.setSalary(salary);
		
		Address address = new Address();
		address.setStreet(street);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		
		em.getTransaction().begin();
		emp.setAddress(address);
		em.persist(emp);
		em.getTransaction().commit();
		
		return emp;
	}
	
	
	public Company createCompanyAndAddress(int id,
			String street, String city, String state, String zip) {
		
		Company c = new Company();
		c.setId(id);
		Address address = new Address();
		address.setStreet(street);
		address.setCity(city);
		address.setState(state);
		address.setZip(zip);
		
		em.getTransaction().begin();
		c.setAdress(address);
		em.persist(c);
		em.getTransaction().commit();
		
		return c;
		
	}
	
	public Collection<Employee> findAllEmployees(){
		Query query = em.createQuery("Select e FROM Employee e");
		return (Collection<Employee>) query.getResultList();
	}
	
	public Collection<Company> findAllCompanies(){
		Query query = em.createQuery("Select c FROM Company c");
		return (Collection<Company>) query.getResultList();
	}
	
}
