package mx.unam.dgtic.dominio;

public enum EmployeeType {

	FULL_TIME_EMPLOYEE,
    PART_TIME_EMPLOYEE,  
    CONTRACT_EMPLOYEE

}
