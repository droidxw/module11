package mx.unam.dgtic.dominio;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Company {
	
	@Id 
	private int id;
	@Embedded
	private Address adress;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Address getAdress() {
		return adress;
	}
	public void setAdress(Address adress) {
		this.adress = adress;
	}
	@Override
	public String toString() {
		return "Company [id=" + id + ", adress=" + getAdress() + "]";
	}
	
	

}
