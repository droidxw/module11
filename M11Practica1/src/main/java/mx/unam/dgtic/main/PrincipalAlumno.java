package mx.unam.dgtic.main;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Alumno;
import mx.unam.dgtic.modelo.AlumnoService;
;

public class PrincipalAlumno {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("m11base");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
		AlumnoService service =new AlumnoService(em);
	
//	solo se ejecuta una vez para que no marque error	
	//llamamos al metodo en el caso de autogenracion de llave para auto
//	service.createAlumno("700","Hector", "delaTorre");
	System.out.println("Hector de la Torre");
	System.out.println("");
		
		Collection<Alumno> alumnos=service.findAllAlumnos();
		
		System.out.println("Employe localizados Local");
		for(Alumno e:alumnos)
		System.out.println(e);
//		System.out.println(e.getConvertedName());
//		System.out.println(e.getName());

		
	}

}
