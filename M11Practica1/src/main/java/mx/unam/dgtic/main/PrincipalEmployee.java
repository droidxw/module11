package mx.unam.dgtic.main;

import java.util.Collection;

import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.modelo.EmployeeService;

public class PrincipalEmployee {
	
	public static void main (String [] args) {
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("arceliabase");
		//administrar ciclo de vida de unidad
		EntityManager em=emf.createEntityManager();
		
	EmployeeService service =new EmployeeService(em);
	
//	solo se ejecuta una vez para que no marque error	
	//llamamos al metodo en el caso de autogenracion de llave para auto
	service.createEmployee("Hector de la Torre", 5000);
	System.out.println("Hector de la Torre");
	System.out.println("");
		
		Collection<Employee> empleados=service.findAllEmployees();
		
		System.out.println("Employe localizados Local");
		for(Employee e:empleados)
		System.out.println(e);
//		System.out.println(e.getConvertedName());
//		System.out.println(e.getName());

		
	}

}
