package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Alumno;

public class AlumnoService {

	protected EntityManager em;

	public AlumnoService(EntityManager em) {	
		this.em = em;
	}
	
	public Alumno createAlumno (Integer matricula, String name, String lastName) {
		
		Alumno emp=new Alumno();
	
		emp.setMatricula(matricula);
		emp.setNombre(name);
		emp.setPaterno(lastName);
		
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;
		
		
	}
	
	
	public Alumno createAlumno ( String name, String lastName) {
		
		Alumno emp=new Alumno();
		//se comenta id al usar una estartegia de autogeneracion de llave

		emp.setNombre(name);
		emp.setPaterno(lastName);
		
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;
		
		
	}
	
	public Collection<Alumno> findAllAlumnos() {
		
		Query query =em.createQuery("Select e FROM Alumno e");
		return (Collection<Alumno>)query.getResultList();
	}
	
	
}
