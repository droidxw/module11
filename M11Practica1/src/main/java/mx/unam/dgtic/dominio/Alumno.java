package mx.unam.dgtic.dominio;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;


@Entity
//@Table(name="Alumnos")

public class Alumno {
	
	@Id
	private Integer matricula;
	private String nombre;
	private String paterno;

	
	@Transient
	private String concatenatedName;
	

	private AlumnoType type;
	
	@Enumerated(EnumType.STRING)
	private AlumnoType previousType;
	
	
	
	
	public Alumno(Integer matricula) {
		super();
		this.matricula = matricula;
	}
	public Alumno() {
		super();
	}


public Alumno(Integer matricula, String nombre, String paterno) {
	super();
	this.matricula = matricula;
	this.nombre = nombre;
	this.paterno = paterno;

}

public Alumno(String nombre, String paterno) {
	super();
	
	this.nombre = nombre;
	this.paterno = paterno;

}
	
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	
	public String getConcatenatedName() {
		concatenatedName =ConcatenatedName(this.paterno);
		return concatenatedName;
	}
//	public void setConcatenatedName(String convertedName) {
//		this.convertedName = convertedName;
//	}
	
	protected String ConcatenatedName(String paterno) {
		return nombre.concat(""+paterno);
	}
	

	
	
	public AlumnoType getType() {
		return type;
	}
	public void setType(AlumnoType type) {
		this.previousType=this.type;
		if(this.previousType==null) {
			this.previousType=type;
		}
		this.type = type;
	}
	public AlumnoType getPreviousType() {
		return previousType;
	}
	public void setPreviousType(AlumnoType previousType) {
		this.previousType = previousType;
	}
	
	@Override
	public String toString() {
		return "Alumno [matricula=" + matricula + ", nombre=" + nombre + ", paterno=" + paterno+ ", nombreConcatenado="
		+ getConcatenatedName()+" type="+ type
				
		+ "]";
	}
	

	
	
}