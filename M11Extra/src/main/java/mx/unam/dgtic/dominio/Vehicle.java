package mx.unam.dgtic.dominio;

import java.util.Collection;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinColumns;
import jakarta.persistence.ManyToOne;
import mx.unam.dgtic.modelo.VehicleService;

@Entity
public class Vehicle {
	
	 @Id
	@Column(name = "id_vehiculo")
	private Integer id;
	private String marca;
	private String modelo;
	private String anio;
	private String serie;
	@Column(name = "id_cliente")
	private String idcliente;
//	@ManyToOne
	@ManyToOne(targetEntity = Client.class,fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
//	@JoinColumns
//	@JoinColumns(name = "id_cliente", nullable = false, value = { @JoinColumn })
	@JoinColumn(name = "id_cliente", nullable = false, insertable=false, updatable=false)
	private Client clients; 
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getId_cliente() {
		return idcliente;
	}
	public void setId_cliente(String idcliente) {
		this.idcliente = idcliente;
	}
	@Override
	public String toString() {
		return "Vehicle [id=" + id + ", marca=" + marca + ", modelo=" + modelo + ", anio=" + anio + ", serie=" + serie
				+ ", id_cliente=" + idcliente + "]";
	}




    
    
}
