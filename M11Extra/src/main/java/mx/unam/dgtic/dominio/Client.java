package mx.unam.dgtic.dominio;

import java.util.Collection;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Client {
    @Id
	@Column(name = "id_cliente")
	private Integer id;

	private String nombre;

	private String apellido;

	private String email;	

	private String genero;
	
	@OneToMany(mappedBy="clients",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Collection<Vehicle> vehicles; 
	
//	private Set<Vehicle> vehicle;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    

	public Collection<Vehicle> getVehicles() {
		return vehicles;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", email=" + email + ", genero="
				+ genero + "]";
	}


	
	public void addVehicle(Vehicle vehicle) {
		if(!getVehicles().contains(vehicle)) {
			getVehicles().add(vehicle);
			
		}
//		if(!vehicle.getId_cliente().ccontains(this)) {
//			vehicle.getVehicles().add(this);
//		}

		}


    
 


    
}
