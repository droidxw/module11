package mx.unam.dgtic.main;

import java.util.Collection;

import javax.xml.transform.SourceLocator;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Client;
import mx.unam.dgtic.dominio.Employee;
import mx.unam.dgtic.dominio.Vehicle;
import mx.unam.dgtic.modelo.ClientService;
import mx.unam.dgtic.modelo.EmployeeService;
import mx.unam.dgtic.modelo.VehicleService;

public class PrincipalClient {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("concesionaria");
		// administrar ciclo de vida de unidad
		EntityManager em = emf.createEntityManager();

		ClientService cservice = new ClientService(em);

		VehicleService vservice = new VehicleService(em);

// 	EmployeeService service =new EmployeeService(em);

// //	solo se ejecuta una vez para que no marque error	
// 	//llamamos al metodo en el caso de autogenracion de llave para auto
		Client emp=cservice.createClient(36,"Hector", "de la Torre", "hectordtp@gmail.com","Male");
 	vservice.createVehicle(36, "Hyundai", "TRX", "1995", "RX", "36");
 	System.out.println("Hector de la Torre");
 	System.out.println("Cliente creado");
		
		
//	xxx	emp=cservice.setClient(4, 4);

//		service quita al remover department
//		Client cli=cservice.setClient(emp, 36);
//		xxxxemp=cservice.setClient(0, 0);
			
//			service.setEmployeeProject(200, 5);


		
		
// 		Collection<Employee> empleados=service.findAllEmployees();

// 		System.out.println("Employe localizados Local");
// 		for(Employee e:empleados)
// 		System.out.println(e);
// //		System.out.println(e.getConvertedName());
// //		System.out.println(e.getName());

		Collection<Client> clients = cservice.findAllClients();

		System.out.println("Clientes localizados Local");
		for (Client e : clients) {
			System.out.println(e);
		}
		

		Collection<Vehicle> vehicles = vservice.findAllVehicles();

		System.out.println("Vehiculos localizados Local");
		for (Vehicle e : vehicles) {
			System.out.println(e);

		}
		
		
		Client update = cservice.updateClient(36, "test@gmail.com" );
		System.out.println("Cliente actualizado Local");
		
		System.out.println(update);
		
		
		
		cservice.removeClient(36);
		System.out.println("Cliente removido Local");
	
	}
}
