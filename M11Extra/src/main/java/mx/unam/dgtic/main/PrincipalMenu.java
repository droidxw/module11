package mx.unam.dgtic.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Date;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
//import mx.unam.dgtic.dominio.Alumno;
import mx.unam.dgtic.dominio.Client;
import mx.unam.dgtic.dominio.Vehicle;
//import mx.unam.dgtic.modelo.AlumnoService;
import mx.unam.dgtic.modelo.ClientService;
import mx.unam.dgtic.modelo.VehicleService;

public class PrincipalMenu {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("concesionaria");
		// administrar ciclo de vida de unidad
		EntityManager em = emf.createEntityManager();
		
		ClientService cservice = new ClientService(em);

		VehicleService vservice = new VehicleService(em);
		
		Client client;
		
		Vehicle vehicle;

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String ac;
		String matricula;
		String num;
		Integer id;
		String mail;

		try {
			while (true) {
				System.out.println("\n HECTOR DE LA TORRE \n");
				System.out.println("\n[L]istar | [D]esplegarId | [A]gregar | [M]odificar | [R]emover | [S]alir \n");
				ac = in.readLine();
				if ((ac.length() == 0) || ac.toUpperCase().charAt(0) == 'S') {
					System.out.println("Bye");
					break;
				}

				switch (ac.toUpperCase().charAt(0)) {
			
				case 'L':
					Collection<Client> clients = cservice.findAllClients();
					System.out.println("Clientes localizados Local");
					for (Client e : clients) {
						System.out.println(e);
					}
					break;


				case 'A':
					System.out.println("Id: ");
					num = in.readLine();
					id=Integer.valueOf(num);
//					System.out.println("Id: "+id );
					System.out.println("Nombre: ");
					String nombre = in.readLine();
					System.out.println("Paterno: ");
					String paterno = in.readLine();
					System.out.println("CorreoElectronico: ");
					 mail = in.readLine();
					System.out.println("Genero: ");
					String genero = in.readLine();	
					
					System.out.println("id Auto: ");
					String idA=in.readLine();
					Integer idauto = Integer.valueOf(idA);
					System.out.println("Marca: ");
					String marca = in.readLine();
					System.out.println("Modelo: ");
					String modelo = in.readLine();
					System.out.println("Año: ");
					String anio= in.readLine();
					System.out.println("Serie: ");
					String serie= in.readLine();
					
					

					client =cservice.createClient(id,nombre, paterno, mail, genero);
					vehicle=vservice.createVehicle(idauto,marca, modelo, anio, serie, num);
					System.out.println("Cliente agregado: " + client+" "+vehicle);

					break;		
					
					

				case 'M':

					System.out.println("Id del cliente: ");
					num = in.readLine();
					id=Integer.valueOf(num);
					System.out.println("CorreoElectronico: ");
					mail = in.readLine();
					System.out.println("Cliente: " + mail);
					client =cservice.updateClient(id, mail);

					System.out.println("Id nuevo: ");
					String numN = in.readLine();
//					Integer idN=Integer.valueOf(num);
					vehicle=vservice.updateVehicle(id, numN);
					System.out.println("Modificado: " + client+ vehicle);


					break;
					

				case 'R':
					System.out.println("Id a eliminar: ");
					num = in.readLine();
					id=Integer.valueOf(num);
					cservice.removeClient(id);
					System.out.println("Cliente eliminado: " + id);
					break;
					
					
				case 'D':
					System.out.println("Id a mostrar: ");
					num = in.readLine();
					id=Integer.valueOf(num);
					client =cservice.findClient(id);
					vehicle=vservice.findVehicle(id);
					System.out.println("Informacion de Cliente " + client+""+vehicle);
					break;


				default:
					continue;
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
			// TODO: handle exception

		} finally {
			em.close();
			emf.close();
		}

	}

}
