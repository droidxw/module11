package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Client;
import mx.unam.dgtic.dominio.Vehicle;

public class ClientService {

    protected EntityManager em;

    public ClientService(EntityManager em) {	
        this.em = em;
    }


    public Client createClient (int id, String name, String apellido, String email, String genero ) {
		
		Client emp=new Client();
		
	
		emp.setId(id);
		emp.setNombre(name);
		emp.setApellido(apellido);       
        emp.setEmail(email);
        emp.setGenero(genero);
        
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;
		
		
	}
    
    
    
public Client updateClient(Integer matricula, String mail) {
		
		Client client =findClient(matricula);
		
		
		if (client!=null) {
			
			client.setEmail(mail);

		}
		
		em.getTransaction().begin();
		em.persist(client);
		em.getTransaction().commit();
		
		return client;
	}
    
    
//	public Client setClient(int idClient, int idVehicle) {
//		Client e = em.find(Client.class, idClient);
//		Vehicle p = em.find(Vehicle.class, idVehicle);
//		em.getTransaction().begin();
//		e.addVehicle(p);
//		em.getTransaction().commit();
//		return e;
//	}


    public Collection<Client> findAllClients() {
        Query query =em.createQuery("Select e FROM Client e");
		return (Collection<Client>)query.getResultList();
       

	}
    
    public  Client findClient(Integer id) {
		return em.find(Client.class, id);
		
	}
    
    
	
	public void removeClient(Integer id) {
		Client cliente =findClient(id);
		if (cliente!=null) {
			
			em.getTransaction().begin();
			em.remove(cliente);
			em.getTransaction().commit();
			
		}		
	
		return;
	}
    
}
