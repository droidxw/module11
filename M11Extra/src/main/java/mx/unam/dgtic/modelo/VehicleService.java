package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Client;
import mx.unam.dgtic.dominio.Vehicle;

public class VehicleService {
	
	   protected EntityManager em;

	    public VehicleService(EntityManager em) {	
	        this.em = em;
	    }
	    
	    
	    public Vehicle createVehicle(int id, String marca, String modelo, String anio, String serie, String idcliente) {
			
	    	Vehicle emp=new Vehicle();
			
		
			emp.setId(id);
			emp.setMarca(marca);
			emp.setModelo(modelo);       
	        emp.setAnio(anio);
	        emp.setSerie(serie);
	        emp.setId_cliente(idcliente);
	        
			em.getTransaction().begin();
			em.persist(emp);
			em.getTransaction().commit();
			return emp;
			
			
		}
	    
	    
	    public Vehicle updateVehicle(Integer matricula, String nueva) {
			
	    	Vehicle vehicle=findVehicle(matricula);
			
			
			if (vehicle!=null) {
				
				vehicle.setId_cliente(nueva);

			}
			
			em.getTransaction().begin();
			em.persist(vehicle);
			em.getTransaction().commit();
			
			return vehicle;
		}
	    
//	    
//		public Vehicle setVehicle(int idClient, int idVehicle) {
//			Client e = em.find(Client.class, idClient);
//			Vehicle p = em.find(Vehicle.class, idVehicle);
//			em.getTransaction().begin();
//			e.addVehicle(p);
//			em.getTransaction().commit();
//			return e;
//		}
//	    
	    
	    
	    public Collection<Vehicle> findAllVehicles() {
	        Query query =em.createQuery("Select e FROM Vehicle e");
			return (Collection<Vehicle>)query.getResultList();
	       

		}
	    
	    
	    public  Vehicle findVehicle(Integer id) {
			return em.find(Vehicle.class, id);
			
		}
	    

}
