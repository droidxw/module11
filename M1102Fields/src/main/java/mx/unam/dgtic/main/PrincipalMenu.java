package mx.unam.dgtic.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Date;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import mx.unam.dgtic.dominio.Alumno;
import mx.unam.dgtic.modelo.AlumnoService;

public class PrincipalMenu {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("m11base");
		// administrar ciclo de vida de unidad
		EntityManager em = emf.createEntityManager();

		AlumnoService service = new AlumnoService(em);
		Alumno alumno;

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String ac;
		String matricula;

		try {
			while (true) {
				System.out.println("\n HECTOR DE LA TORRE \n");
				System.out.println("\n[L]istar | [A]gregar | [M]odificar | [R]emover | [S]alir \n");
				ac = in.readLine();
				if ((ac.length() == 0) || ac.toUpperCase().charAt(0) == 'S') {
					System.out.println("Bye");
					break;
				}

				switch (ac.toUpperCase().charAt(0)) {
				case 'L':
					Collection<Alumno> alumnos = service.findAllAlumnos();

					System.out.println("Alumnos localizados Local");
					for (Alumno a : alumnos)
						System.out.println(a);
					break;

				case 'A':
					System.out.println("Matricula: ");
					matricula = in.readLine();
					System.out.println("Matricula: "+matricula );
					System.out.println("Nombre: ");
					String nombre = in.readLine();
					System.out.println("Paterno: ");
					String paterno = in.readLine();
					System.out.println("Estatura: ");
					Double estatura = Double.parseDouble(in.readLine());
					em.getTransaction().begin();
					alumno = service.createAlumno(matricula, nombre, paterno, new Date(), estatura);
					em.getTransaction().commit();
					System.out.println("Alumno agregado: " + alumno);
					break;

				case 'M':

					System.out.println("Matricula a modificar: ");
					matricula = in.readLine();
					alumno = service.findAlumno(matricula);

					System.out.println("Alumno: " + alumno);
					em.getTransaction().begin();
					alumno = service.updateAlumno(matricula, 1);
					em.getTransaction().commit();
					System.out.println("Modificado: " + alumno);

					break;

				case 'R':

					System.out.println("Matricula a eliminar: ");
					matricula = in.readLine();
					em.getTransaction().begin();
					service.removeAlumno(matricula);
					em.getTransaction().commit();
					System.out.println("Eliminado: " + matricula);

					break;

				default:
					continue;
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
			// TODO: handle exception

		} finally {
			em.close();
			emf.close();
		}

	}

}
