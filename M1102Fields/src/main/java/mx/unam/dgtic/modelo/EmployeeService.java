package mx.unam.dgtic.modelo;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import mx.unam.dgtic.dominio.Employee;

public class EmployeeService {

	protected EntityManager em;

	public EmployeeService(EntityManager em) {	
		this.em = em;
	}
	
	public Employee createEmployee (int id, String name, long salary) {
		
		Employee emp=new Employee();
	
		emp.setId(id);
		emp.setName(name);
		emp.setSalary(salary);
		
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;
		
		
	}
	
	
	public Employee createEmployee ( String name, long salary) {
		
		Employee emp=new Employee();
		//se comenta id al usar una estartegia de autogeneracion de llave

		emp.setName(name);
		emp.setSalary(salary);
		
		em.getTransaction().begin();
		em.persist(emp);
		em.getTransaction().commit();
		return emp;
		
		
	}
	
	public Collection<Employee> findAllEmployees() {
		
		Query query =em.createQuery("Select e FROM Employee e");
		return (Collection<Employee>)query.getResultList();
	}
	
	
}
