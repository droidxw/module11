package mx.unam.dgtic.modelo;

import java.text.CollationElementIterator;
import java.util.Date;

import java.util.Collection;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import mx.unam.dgtic.dominio.Alumno;

public class AlumnoService {
	//metodos para manipular clase alumno
	//ciclo vida entidad
	protected EntityManager em;

	public AlumnoService(EntityManager em) {		
		this.em = em;
	}
	
	public Alumno createAlumno (String matricula, String nombre, String paterno, Date fnac, double estatura) {
		Alumno alumno=new Alumno(matricula);
		alumno.setNombre(nombre);
		alumno.setPaterno(paterno);
		alumno.setFnac(fnac);
		alumno.setEstatura(estatura);
		
		em.persist(alumno);
		return alumno;
	}
	
	public void removeAlumno(String matricula) {
		
		Alumno alumno =findAlumno(matricula);
		if (alumno!=null) {
			em.remove(alumno);
		}
	
	}
	
	
	public  Alumno findAlumno(String matricula) {
		return em.find(Alumno.class, matricula);
		
	}
	
	public Alumno updateAlumno(String matricula, long raise) {
		
		Alumno alumno =findAlumno(matricula);
		
		
		if (alumno!=null) {
			alumno.setEstatura(alumno.getEstatura() + raise);
		}
		
		return alumno;
	}
	
	public Collection<Alumno> findAllAlumnos(){
		TypedQuery<Alumno> query =em.createQuery("SELECT a FROM Alumno a", Alumno.class);
		return query.getResultList();
		
	}
 
}
