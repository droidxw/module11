package mx.unam.dgtic.dominio;

import java.util.ArrayList;
import java.util.Collection;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;

	@OneToMany(mappedBy = "department")
	private Collection<Employee> employees;

	public Department() {
		employees = new ArrayList<Employee>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Employee> getEmployees() {
		return employees;
	}

//		public void setEmployees(Collection<Employee> employees) {
//			this.employees = employees;
//		}

	public void addEmployee(Employee employee) {
		if (!getEmployees().contains(employee)) {

			getEmployees().add(employee);
			if (employee.getDepartment() != null) {
				employee.getDepartment().getEmployees().remove(employee);

			}
			employee.setDepartment(this);
		}

	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + "] ";
	}

}
